var CYA = function() {
    //Private vars

return {
	//Public vars/methods
	init:function(){
		CYA.loadPage();
	},
	
	loadPage:function(){
		$('#mainContent').empty();
		$('#mainContent').load('html/cya.html', function() {
		
			table = $("#eventsTable").dataTable({
				"iDisplayLength": -1,
				"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],			
				"bLengthChange": false,
				"columnDefs": [
					{
						"targets": [ 0 ],
						"visible": false,
						"searchable": false
					}
				]				
			});			
			
			$("#eventsTable_filter").hide();
			$("#eventsTable_info").hide();
			$("#eventsTable_paginate").hide();		
		
			$(".nav_reg").on("click", function (event) {
				event = null;
				currPage = "reg";
				REG.init();
			}); 
		
			$(".nav_fund_ppk").on("click", function (event) {
				event = null;
				currPage = "fund_ppk";
				FUND_PPK.init();
			}); 		
		
			$(".nav_fund_wings").on("click", function (event) {
				event = null;
				currPage = "fund_wings";
				FUND_WINGS.init();
			}); 
		

		});
	}	
}}();