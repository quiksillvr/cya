if (typeof console === "undefined") {
    console = {
        log:function () {
        },
        log:function (msg) {
        }
    };
}

var MAIN = function() {
    //Private vars
	var cyaLayout = "";
	
	function showFootballs(){
	   $('#footballTopDiv').show("slide", { direction: "down" }, 1000);
	   //$('#footballBottomDiv').slideDown('slow');
	   $('#footballBottomDiv').show("slide", { direction: "up" }, 1000);
	}
	
return {
	//Public vars/methods
	init:function(){
		cyaLayout = $('body').layout({
			applyDefaultStyles: false,
			spacing_opened: 0,
			spacing_closed: 0,
			north__closable: true,
			north__enableCursorHotkey: true,
			north__resizable: false,
			north__spacing_open: 5,
			north__spacing_closed: 20,
			north_size: 55,
			south__closable: false,
			south__enableCursorHotkey: false,
			south__size: 18,
			south__resizable: false,
			south__spacing_open: 0,
			south__spacing_closed: 0,
			//east__resizable: false,
			//east__initHidden: true,
			
			east__closable: false,
			east__enableCursorHotkey: false,
			east__resizable: false,
			east__size: 281,
			east__spacing_open: 0,
			east__spacing_closed: 0, 			
			
			west__closable: true,
			west__enableCursorHotkey: true,
			west__resizable: true,
			west__size: 168,
			west__spacing_open: 5,
			west__spacing_closed: 20 
		});
		$( "#repeat" ).buttonset();
		$( "#menu" ).menu();
		cyaLayout.resizeAll(); 
		NAV.init();
		$('.noShow').removeClass('noShow').fadeIn('slow');
		showFootballs();
	}
}}();
$(document).ready(function () {
	MAIN.init();
});