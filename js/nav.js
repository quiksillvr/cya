/**
 * $Author:M. Roberts
 * $Date: Mar. 9th 2013
 *
 * Description: Contains implementation logic for the CYA Football navigation.
 */
var NAV = (function () {

    //--------------------------------
    // Private Variables and Methods
    //--------------------------------

    var currPage = "cya";

    function tbd() {

    }

    return {
        // -----------------------------
        // Public Variables and Methods
        // -----------------------------

        bindNavClicks: function () {
            
            
            $("#bannerImgDiv").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $("#home_link").addClass('active-nav');
                currPage = "cya";
                $.cookie("currPage", currPage);
                CYA.init();
            });            
            $(".nav_cya").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $(this).addClass('active-nav');
                currPage = "cya";
                $.cookie("currPage", currPage);
                CYA.init();
            });
            $(".nav_board").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $(this).addClass('active-nav');
                currPage = "board";
                $.cookie("currPage", currPage);
                BOARD.init();
            });
            /*$(".nav_contact").on("click", function (event) {
             event = null;
             $('.ui-menu-item a').removeClass('active-nav');
             $(this).addClass('active-nav');				
             currPage = "contact";
             $.cookie("currPage", currPage);
             CONTACT.init();
             });*/
            $(".nav_reg").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $(this).addClass('active-nav');
                currPage = "reg";
                $.cookie("currPage", currPage);
                REG.init();
            });
            /*$(".nav_team_pics_football").on("click", function (event) {
             event = null;
             $('.ui-menu-item a').removeClass('active-nav');
             $(this).addClass('active-nav');				
             currPage = "team_pics_football";
             $.cookie("currPage", currPage);
             TEAM_PICS_FOOTBALL.init();
             });*/
            /*$(".nav_schedule").on("click", function (event) {
             event = null;
             $('.ui-menu-item a').removeClass('active-nav');
             $(this).addClass('active-nav');				
             currPage = "schedule";
             $.cookie("currPage", currPage);
             SCHEDULE.init();
             });*/
            $(".nav_weight").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $(this).addClass('active-nav');
                currPage = "weight";
                $.cookie("currPage", currPage);
                WEIGHT.init();
            });
            $(".nav_directions").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $(this).addClass('active-nav');
                currPage = "directions";
                $.cookie("currPage", currPage);
                DIRECTIONS.init();
            });
            $(".nav_addresses").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $(this).addClass('active-nav');
                currPage = "addresses";
                $.cookie("currPage", currPage);
                ADDRESSES.init();
            });
            /*$(".nav_fund_ppk").on("click", function (event) {
             event = null;
             currPage = "fund_ppk";
             $.cookie("currPage", currPage);
             FUND_PPK.init();
             });			
             $(".nav_fund_wings").on("click", function (event) {
             event = null;
             currPage = "fund_wings";
             $.cookie("currPage", currPage);
             FUND_WINGS.init();
             });*/
            $(".nav_cheer_news").on("click", function (event) {
                event = null;
                $('.ui-menu-item a').removeClass('active-nav');
                $(this).addClass('active-nav');
                currPage = "cheer_news";
                $.cookie("currPage", currPage);
                CHEER_NEWS.init();
            });
        },
        /**
         * Initialization of the CYA Navigation Menu.
         */
        init: function () {
            NAV.bindNavClicks();
            var cookieCurrPage = $.cookie("currPage");
            $('.ui-menu-item a').removeClass('active-nav');
            $(".nav_" + cookieCurrPage).addClass('active-nav');
            if (cookieCurrPage) {
                if (cookieCurrPage == "cya") {
                    CYA.init();
                } else if (cookieCurrPage == "board") {
                    BOARD.init();
                    //}else if(cookieCurrPage == "contact"){
                    //  CONTACT.init();
                } else if (cookieCurrPage == "reg") {
                    REG.init();
                    //}else if(cookieCurrPage == "team_pics_football"){
                    //  TEAM_PICS_FOOTBALL.init();
                    //}else if(cookieCurrPage == "schedule"){
                    //  SCHEDULE.init();
                } else if (cookieCurrPage == "weight") {
                    WEIGHT.init();
                } else if (cookieCurrPage == "directions") {
                    DIRECTIONS.init();
                } else if (cookieCurrPage == "addresses") {
                    ADDRESSES.init();
                    //}else if(cookieCurrPage == "fund_wings"){
                    // FUND_WINGS.init();
                } else if (cookieCurrPage == "cheer_news") {
                    CHEER_NEWS.init();
                }
            } else {
                CYA.init();
            }
        }
    };
})();
